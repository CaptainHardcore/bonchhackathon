package com.itkindaworks.bonchhack;


import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Observable;
import java.util.StringTokenizer;

import rx.Observer;

/**
 * Created by remember me on 20.10.2017.
 */

public class SpeechObserver extends Observable implements Observer<String> {

    int switcher = 0;
    ArrayList<String> answers = new ArrayList<>();

    @Override
    public synchronized void addObserver(java.util.Observer o) {
        super.addObserver(o);
    }

    @Override
    public void onNext(@NonNull String s) {
        String t = "";
        if (s.contains("зыки программирования")){
            t = "программирования";
            parseAnswers(s, t);
        }
    }

    @Override
    public void notifyObservers(Object arg) {
        super.notifyObservers(arg);
    }

    @Override
    public void onCompleted() {

    }

    @Override
    public void onError(@NonNull Throwable e) {

    }
    public void onComplete() {

    }
   void parseAnswers(String s,String t){
       StringTokenizer tokenizer = new StringTokenizer(s);
       while (tokenizer.hasMoreElements()){
          String token = (String) tokenizer.nextElement();
           if (token.equals(t)){
               switcher = 1;
           }else if (switcher == 1){
               if (!token.equals("я")|!token.equals("знаю")){
               answers.add(token);}
       }

     }
     setChanged();
       notifyObservers(answers);

   }

    @Override
    protected synchronized void setChanged() {
        super.setChanged();
    }



}
