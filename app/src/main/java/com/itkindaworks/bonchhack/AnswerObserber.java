package com.itkindaworks.bonchhack;

import rx.Observer;

/**
 * Created by remember me on 20.10.2017.
 */

public class AnswerObserber implements Observer<String> {

    int answer = 0;

    public void onCompleted() {

    }

    @Override
    public void onError(Throwable e) {

    }

    @Override
    public void onNext(String s) {
        switch (s){
            case "lang":
                answer = 1;
                break;
        }
    }
}
