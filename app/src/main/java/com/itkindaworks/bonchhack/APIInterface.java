package com.itkindaworks.bonchhack;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by remember me on 21.10.2017.
 */

public interface APIInterface {

    @POST("api/post")
    Call<Response> postData(@Query("type") String type,
                            @Query("first") double first,
                            @Query("second") double second,
                            @Query("third") double third);
}
